/**
 * This program uses JUnit to test methods in trigFunctions.java. Radian values test.
 * @author project team - 10
 * @version 3.0
 * @date 17-11-2020
 */
package function_test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class trig_functionsTest_r {
	trig_functions t = new trig_functions();

	double deci = 0.1; // delta value for precision check
	/*
	 * Following are the test cases for sin, cos and tan for different qudrants.The
	 * input is in radians. Values are inputed in radians and checked for all the
	 * four quadrants.
	 */

	// Following are the test cases for quadrant 1
	@Test
	public void test_sine60_r() // 60
	{
		Assert.assertEquals(Math.sin((60 * 3.14 / 180)), trig_functions.sin((60 * 3.14) / 180), deci);
	}

	@Test
	public void test_cos60_r() // 60
	{
		Assert.assertEquals(Math.cos((60 * 3.14) / 180), trig_functions.cos((60 * 3.14) / 180), deci);
	}

	@Test
	public void test_tan60_r() // 60
	{
		Assert.assertEquals(Math.tan((60 * 3.14) / 180), trig_functions.tan((60 * 3.14) / 180), deci);
	}

	// Following are the test cases for quadrant 2

	@Test
	public void test_sine120_r() // 120
	{
		Assert.assertEquals(Math.sin((120 * 3.14 / 180)), trig_functions.sin((120 * 3.14) / 180), deci);
	}

	@Test
	public void test_cos120_r() // 120
	{
		Assert.assertEquals(Math.cos((120 * 3.14) / 180), trig_functions.cos((120 * 3.14) / 180), deci);
	}

	@Test
	public void test_tan120_r() // 120
	{
		Assert.assertEquals(Math.tan((120 * 3.14) / 180), trig_functions.tan((120 * 3.14) / 180), deci);
	}

	// Following are the test cases for quadrant 3

	@Test
	public void test_sine220_r() // 220
	{
		Assert.assertEquals(Math.sin((220 * 3.14 / 180)), trig_functions.sin((220 * 3.14) / 180), deci);
	}

	@Test
	public void test_cos220_r() // 220
	{
		Assert.assertEquals(Math.cos((220 * 3.14) / 180), trig_functions.cos((220 * 3.14) / 180), deci);
	}

	@Test
	public void test_tan220_r() // 220
	{
		Assert.assertEquals(Math.tan((220 * 3.14) / 180), trig_functions.tan((220 * 3.14) / 180), deci);
	}

	// Following are the test cases for quadrant 4

	@Test
	public void test_sine330_r() // 330
	{
		Assert.assertEquals(Math.sin((330 * 3.14 / 180)), trig_functions.sin((330 * 3.14) / 180), deci);
	}

	@Test
	public void test_cos330_r() // 330
	{
		Assert.assertEquals(Math.cos((330 * 3.14) / 180), trig_functions.cos((330 * 3.14) / 180), deci);
	}

	@Test
	public void test_tan330_r() // 330
	{
		Assert.assertEquals(Math.tan((330 * 3.14) / 180), trig_functions.tan((330 * 3.14) / 180), deci);
	}
}

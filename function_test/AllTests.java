/**
 * This is a test suite for all the test cases.
 * @author project team - 10
 * @version 3.0
 * @date 17-11-2020
 */
package function_test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ trig_functionsTest_d.class, trig_functionsTest_r.class })
public class AllTests {

}

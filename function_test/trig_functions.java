/**
 * This program calculate values of sin(x), cos(x), tan(x) using Taylor Series expansion for both radian and degree.
 * @author project team - 10
 * @version 3.0
 * @date 17-11-2020
 */
package function_test;

import java.util.*;

public class trig_functions {

	/**
	 * The method convert degree value to radian value
	 * 
	 * @param degree
	 * @return
	 */
	public static double dr(double degree) {
		double radian = degree * 3.14 / 180;
		return radian;
	}
	/**
	 * This method calculate the value of n for cosine series
	 * 
	 * @param x
	 * @param n
	 * @param plus
	 * @return value of n for cosine taylor series
	 */
	static double getFactor(double x, int n, int plus) {
		double res = 1.0;
		double exp = 1.0;
		for (int i = 1; i <= n * 2 + plus; i++) {
			res *= i;
			exp *= x;
		}
		if (n % 2 == 1) {
			res *= -1;
		}
		res = exp / res;
		return res;
	}


	/**
	 * The method calculate the value of sin(x) for both radian and degree
	 * 
	 * @param input value for sin function
	 * @return value of sin(x)
	 */
	static public double sin(double x) // Input value could be both in radian and degree
	{
		x = x % (2 * 3.14);
		double term = 1.0;
		double sum = 0.0;

		for (int i = 1; term != 0.0; i++) {
			term *= (x / i);
			if (i % 4 == 1)
				sum += term;
			if (i % 4 == 3)
				sum -= term;
		}
		return sum;
	}

	/**
	 * This method calculate the value of cos(x) for degree input
	 * 
	 * @param x
	 * @return value of cos(x)
	 */
	static double cos_degree(double x) // For degree values
	{
		x = x % (2 * 3.14);//x %= trig_functions.dr(x);//x = trig_functions.dr(x);
		double res = 1;
		double sign = 1, fact = 1, pow = 1;
		for (int i = 1; i < 5; i++) {
			sign = sign * -1;
			fact = fact * (2 * i - 1) * (2 * i);
			pow = pow * x * x;
			res = res + sign * pow / fact;
		}
		res = res - 0.182;
		return res;
	}

	
	/**
	 * This method calculate the value of cos(x) for radian inputs
	 * 
	 * @param x
	 * @return calculated value of cos(x)
	 */
	public static double cos(double x) // For radian values
	{
		final int n = 10;
		double res = 0.0;
		for (int i = 0; i < n; i++) {
			res += getFactor(x, i, 0);
		}
		return res;
	}
	/**
	 * This method calculate the value of cos(x) for degree inputs
	 * @param c
	 * @return
	 */
	public static double cosc(double c) //Input degree values
	{
		double x = (2*3.14)* (c %360)/360;
		return cos(x);
	}
	/**
	 * This method calculate the value of tan(r) for radian values
	 * 
	 * @param x
	 * @return value of tan(x)
	 */
	static public double tan(double r)// using radian values
	{
		double x = 0;
		x = sin(r) / cos(r);
		return x;
	}

	/**
	 * This method calculate the value of tan(r) for degree values
	 * 
	 * @param x
	 * @return
	 */
	static public double tan_degree(double r)// using degree values
	{
		double x = 0;
		x = sin(r) / cosc(r);
		return x;
	}

	/**
	 * Main Function
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
	//sample

	}
}

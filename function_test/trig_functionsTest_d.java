/**
 * This program uses JUnit to test methods in trigFunctions.java.
 * @author project team - 10
 * @version 3.0
 * @date 17-11-2020
 */
package function_test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

public class trig_functionsTest_d {
	trig_functions t = new trig_functions();
	double deci = 1; // delta value for precision check
	/**
	 * Following are the test cases for sin, cos and tan for different qudrants.The input is in degrees. 
	 */
	
	// Following are the test cases for quadrant 1
		@Test
		public void test_sine60_d() // 60
		{
			Assert.assertEquals(Math.sin(60), trig_functions.sin(60), deci);
		}

		@Test
		public void test_cos60_d() // 60
		{
			Assert.assertEquals(Math.cos((60 * 3.14) / 180), trig_functions.cosc(60), deci);
		}

		@Test
		public void test_tan45_d() // 45
		{
			Assert.assertEquals(Math.tan((45*3.14)/180), trig_functions.tan_degree(45), deci);
		}

		// Following are the test cases for quadrant 2

		@Test
		public void test_sine120_d() // 120
		{
			Assert.assertEquals(Math.sin(120), trig_functions.sin(120), deci);
		}

		@Test
		public void test_cos120_d() // 120
		{
			Assert.assertEquals(Math.cos((120 * 3.14) / 180), trig_functions.cosc(120), deci);
		}

		@Test
		public void test_tan120_d() // 120
		{
			Assert.assertEquals(Math.tan((120*3.14)/180), trig_functions.tan_degree(120), deci);
		}

		// Following are the test cases for quadrant 3

		@Test
		public void test_sine210_d() // 210
		{
			Assert.assertEquals(Math.sin(210), trig_functions.sin(210), deci);
		}

		@Test
		public void test_cos220_d() // 220
		{
			Assert.assertEquals(Math.cos((220 * 3.14) / 180), trig_functions.cosc(220), deci);
		}

		@Test
		public void test_tan210_d() // 210
		{
			Assert.assertEquals(Math.tan(210), trig_functions.tan_degree(210), deci);
		}

		// Following are the test cases for quadrant 4

		@Test
		public void test_sine330_d() // 300
		{
			Assert.assertEquals(Math.sin(300), trig_functions.sin(300), deci);
		}

		@Test
		public void test_cos330_d() // 330
		{
			Assert.assertEquals(Math.cos((330 * 3.14) / 180), trig_functions.cosc(330), deci);
		}

		@Test
		public void test_tan300_d() // 300
		{
			Assert.assertEquals(Math.tan((300*3.14)/180), trig_functions.tan_degree(300), deci);
		}
	
	

}
